
#ifndef CPS1012ASSIGNMENT_EGGSHELLHEAD_H
#define CPS1012ASSIGNMENT_EGGSHELLHEAD_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/wait.h>
#include "linenoise.h"
#include <ctype.h>
#include <fcntl.h>
#include <signal.h>

#define MAX_STRING 500
#define MAX_ARGS 50
#define MAX_HISTORY 50
#define MAX_PATHS 50

/* TypeDef Structs in VariableSource.c */
typedef struct Variable Var;
typedef struct dynamicArray Arr;
Arr * VarArr;

/* Methods in VariableSource.c */
bool valName(char * name);
void addVariable(char * name, char * data);
int searchByVarName(char * name);
void initVar();
void CWD();
void PROMPT();
void SHELL();
char * returnVarData(char * Vname);
void printAllVar();
void writeToVar(char * NewName, char * NewData);
void freeMem();
void removeVarByName(char * name);

/* Methods in IO_PipeSource */
void checkForPipe(char * args[], int tokenIndex);
void pipeline(char * arr[MAX_ARGS][MAX_ARGS], int noPipes);
void runCMD(char * arr[MAX_ARGS][MAX_ARGS]);
void checkIO(char * args[MAX_ARGS]);
void redirOut(char * name, int redir, char * cmd[MAX_ARGS]);
void redirInput(char *name, char * cmd[MAX_ARGS]);

/* Methods in InternalCmdSource */
void parseCmd(char * args[MAX_ARGS]);
void runSourceCMD(char * args[]);

/* Methods in externalCmdSource */
char * getPath(char * name);
void runExternalCmd(char * args[MAX_STRING]);

#endif //CPS1012ASSIGNMENT_EGGSHELLHEAD_H
