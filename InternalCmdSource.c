//
// Created by benja on 5/12/2020.
//

#include "eggShellHead.h"

//Input: Array of pointers to cmds
//Output: \
//Function: Recognises and runs internal commands passed via array of arguments
void parseCmd(char * args[MAX_STRING])
{
    /* Variable Assignment */
    if(strstr(args[0],"=")>0 && args[1]==NULL)//Searches for = within cmd as indication of variable assignment by user.
    {
        char * arr[2];

        char * token =  strtok(args[0],"=");
        for (int i =0;i<2 && token != NULL;i++)
        {
            arr[i]=token;
            //printf("%s\n",arr[i]);
           token = strtok(NULL, " ");
        }

        //printf("%s\n",arr[1]);
        //printf("%s\n",arr[0]);
        char * temp = malloc(MAX_STRING* sizeof(char));
        char * temp2 = malloc(MAX_STRING* sizeof(char));

        strcpy(temp,arr[0]);
        strcpy(temp2,arr[1]);

        addVariable(temp,temp2);
        printf("%s\n",returnVarData(temp));
        printf("\n");
        return;
    }

    /*Print All Variables*/
    else if(strcmp(args[0], "showenv")==0)
    {
        printAllVar();
        printf("\n");
        return;
    }
    
    /* Echo */ 
    else if(strcmp(args[0],"echo")==0)
    {
        for (int i=1;i<MAX_STRING && args[i] != NULL;i++)
        {
            if(strstr(args[i],"$")!=NULL)
            {
                char arr[MAX_STRING];
                strcpy(arr,args[i]);

              printf("%s ", returnVarData(arr)) ;
              fflush(stdout);
            }else
            {
                printf("%s ",args[i]);
                fflush(stdout);
            }
            
        }
        printf("\n");
        fflush(stdout);
        return;
    }
     
    /*Source Command*/
    else if(strcmp(args[0],"source")==0)
    {
        runSourceCMD(args);
        printf("\n");
    }
    
     /* Change Directory */
    else if(strcmp(args[0],"cd")==0)
    {
        if(args[1]==NULL)
        {
            printf("No other argument passed, Current Directory left unchanged.\n");
            fflush(stdout);
            return;
        }else if(strcmp(args[1],"..")==0 && args[2]==NULL)// Access Parent Directory
        {
          chdir("..");//sets env variable CWD to parent.
          CWD();//updates local CWD
        }else
        {
            char * temp = malloc(MAX_STRING* sizeof(char));
            strcpy(temp,args[1]);
            writeToVar("CWD",temp);
            printf("\n");
            return;
        }
    }

    /* Unset */
    else if (strcmp(args[0],"unset")==0)
    {
        removeVarByName(args[1]);
        printf("\n");
    }
    
    /*Exit Program */
    else if (strcmp(args[0],"exit")==0)//exits the program
    {
        printf("Exiting program with EXITCODE: %s\n",returnVarData("EXITCODE"));
        exit(0);
    }else{
        //printf("passing to external cmd\n");
        //fflush(stdout);
    	runExternalCmd(args);
    }
}

//Input: array of arguments
//Output:\
//Function: Responsible for running source cmd. REMEMBER cap pipes to 1
void runSourceCMD(char * args[])
{
    if (strcmp(args[0],"source")==0 && args[2] != NULL)//if source 
    {
        printf("Invalid Command, enter one and only one filename!\n");
        writeToVar("EXITCODE", "-1");
        return;
    }

    FILE *fp;//creating file pointer

    if((fp = fopen(args[1],"r"))==NULL)
    {
        printf("Unable to open file!\n");
        addVariable("EXITCODE", "-1");
        return;
    }else{//File Opens

        /* Setting variables */

        char line[MAX_STRING];
        char * curr=NULL;
        char * lineArgs[MAX_ARGS];
        char * currLine = NULL;
        char * currLine2=NULL;
        int pipe=0;//used for capping pipes to
        bool mall0c = false;//boolean variable to keep track of whether currline is assigned

        while (fgets(line, sizeof(lineArgs),fp) != NULL)//passes first line from opened file into the line array.
        {
            currLine = malloc(MAX_STRING* sizeof(char));
            currLine2 = malloc(MAX_STRING * sizeof(char));
            mall0c = true;
            
               if(line[ strlen(line)-1 ] == '\n'){
                line[ strlen(line)-1 ] = '\0';
            }

             strcpy(currLine, line);//point currLine to line.
             strcpy(currLine2,line);

            if (strcmp(currLine,"")==0)
            {
                continue; // pass to next iteration.
            }else
            {
                char * currPipe = NULL;
               currPipe = strtok(currLine," ");//tokenise by space  

                for (int i=0;i<MAX_STRING && currPipe != NULL;i++)
                {
                    if (strcmp(currPipe, "|")==0)
                    {
                        pipe++;//counts pipes per line.
                    }

                    if(pipe>1)//ensures that the Pipes per line in the source file are limited to 1.
                    {
                        printf("Pipes per line in source limited to 1\n");
                        fflush(stdout);
                        writeToVar("EXITCODE","-1");
                        return;
                    }
                    currPipe = strtok(NULL," ");

                    
                }
                //printf("%d\n",pipe );
                pipe =0;//resets pipe to 0 for use in next iteration.

               


                curr = strtok(currLine2," ");//tokenise by space


                int j;//token index

                for (j = 0; curr != NULL && j<MAX_STRING-1;j++)//parse through putting commands pointed to by curr into the lineArgs array.
                {
                    lineArgs[j]=curr;
                    curr = strtok(NULL, " ");//next token
                }
                
               // printf("%d\n",j);

                //printf("%s\n",lineArgs[0]);
                //printf("%s\n",lineArgs[1]);
                lineArgs[j]= NULL; //this makes sure that when passed to other functions those functions can find the end of the line.

                checkForPipe(lineArgs,j);//passes on to execute commands.
                memset(line,0,MAX_STRING);//resets line.
            }
        }

        if (mall0c == true)
        {
            free(currLine);//frees currline pointer after use.
        }

    }
}