//
// Created by benja on 5/20/2020.
//

#include "eggShellHead.h"

//Input: filename(for specific external command)
//Output: char pointer to successful path or "fail" if one isnt found.
//Function: see Output
//NB: edits made by user to the programs local variable PATH will affect the functionality of this piece of code.
char * getPath(char * name)
{
    char * allPaths[MAX_PATHS]={""};
    char * path = returnVarData("PATH"); //gets string of all possible root directories delimited by (:).
    char buffer[MAX_STRING]={""};//Buffer used to safeguard 'path' from havoc due to strtok.
    strcpy(buffer, path);//copying path pointed to by the 'path' variable

    char *curr = NULL;//variable for current path initialised to NULL
    //Tokenises contents of buffer using : as a delimiter.
    curr = strtok(buffer,":");
    int j=0;

    for (j; curr != NULL ;j++)
    {
        allPaths[j] = malloc(MAX_STRING * sizeof(char));//allocates memory for strings to be pointed to by the paths array of pointers.

        strcpy(allPaths[j], curr); //copies string pointed to by curr into paths[j].

        //concatenating the rest of the filename to the path.
        strcat(allPaths[j], "/");
        strcat(allPaths[j],name);
         
        //getting next token.
        curr = strtok(NULL,":");
    }

   for (int k = 0; k<MAX_PATHS && allPaths[k] != NULL ;k++)
    {
        //if valid path found, function returns that paths pointer.
        if(access(allPaths[k],X_OK)==0)//If access returns success (0) then that path is deemed valid and so a pointer to it is returned.
        {
            return allPaths[k];
        }//NB: X_OK ensures file is accessible and user has all the necessary privileges to execute it.
    }
    return "fail";

}



//Input: array of pointers to arguments.
//Outout:\
//Function: controls the running of external commands.
void runExternalCmd(char * args[MAX_STRING])
{
    extern char **environ;//pointer to environ (for use with execve)

    char Path[MAX_STRING] = "";//array for storing first found accessible path to command.
    strcpy(Path,getPath(args[0]));//fills allPaths array with all possible paths to command, then checks and returns the first successful path found.
     
    if (strcmp(Path, "fail")==0)
    {

        printf("Couldnt Find Successfull Path or Command Doesn't exist.\n");
        fflush(stdout);
        return;
    }


    /*Fork-Exec Pattern*/

    pid_t pid = fork();//forks new process


    if (pid == 0)//if child is accessed.
    {    
      if(execve(Path,args,environ)==-1)//Caters for failure of execve.
      {
        printf("Unsuccessfull run of command\n");
        writeToVar("EXITCODE","-1");
        exit(-1);
      }
    exit(0);
    }else if (pid > 0)//If parent node accessed.
    {
        int status;

        if(waitpid(pid, &status, WUNTRACED)<0)
        {
            printf("Error occurred whilsts waiting for the child process.\n");
            fflush(stdout);
            return;
        }

        if(WIFEXITED(status))
        {
            return;
        }
        else if(status == -1)
        {
            printf("Failure in the execution of child process.\n");
            writeToVar("EXITCODE", "-1");
            return;
        }
    }

    return;

}