#include "eggShellHead.h"

int main() {

    //Declaring variables needed for operation of the eggShell
    char * line,
         * token= NULL,
         * args[MAX_ARGS];

    int tokenIndex;//counter for tokens.

    
    initVar();//Calling all functions needed to set up environment for the eggshell.
    linenoiseHistorySetMaxLen(MAX_HISTORY);//Allows for use of UP arrow to get previously inputted commands.


    linenoiseClearScreen();//clears screen
    printf("      Welcome to the EggShell CLI.      \n");
    printf("--Enter commands after the prompt below!--\n");
    printf("\n");
    while ((line = linenoise(returnVarData("$PROMPT"))) != NULL)
    {
            if (strcmp(line, "") == 0)//If no command is entered.
             {
                printf("No command was entered! \n");
                continue;

            } else {

                linenoiseHistoryAdd(line);//Adds inputted command to History.

                //begins retrieving tokens using space as a delimiter.
                token = strtok(line, " ");

                for (tokenIndex = 0; token != NULL && tokenIndex < MAX_ARGS - 1; tokenIndex++) //loops through each individual token in entered command.
                {

                    args[tokenIndex] = token;//Stores each token seperately for processing.
                    token = strtok(NULL, " ");//sets token back to NULL so as ready to recieve next token.

                }

                
                args[tokenIndex] = NULL;//Setting last token to NULL
                checkForPipe(args,tokenIndex);//passes inputted command to Pipe parser.

            fflush(stdout);
            printf("\n");
            }

        
        linenoiseFree(line);//Freeing allocated memory.
    }
    freeMem();
    return 0;
}