
#include "eggShellHead.h"

/*Structure for Variable and Dynamic Array for all shell Variables*/
typedef struct Variable
{
   char *name;
   char *data;
}Var;

typedef struct dynamicArray
{
    Var ** arr;
    int varNo; //variable for the current number of stored variables
}Arr;

Arr * VarArr;

/* Methods */

//Input: pointer to variable name
//Output: true or false (valid = true, invalid=false)
//Function: (Validating variable name ) runs through inputted variable name ensures that all characters are legal. Returns false if not.
bool valName (char * name) {
    int size = strlen(name);
    char tempArr[size];
    strcpy(tempArr, name);
    bool outcome = true;

    for (int j = 0; j < size; j++)
    {
            if(tempArr[j] != '_' && !isalnum(tempArr[j]))
            {
                outcome = false;
                return outcome;
            }
    }
    return outcome;
}

//Input: Pointer to variable for name and data.
//Output:\
//Function: Adds a variable to the array, performing validation.
void addVariable(char * name, char * data)
{
    int current = VarArr->varNo;

    if(strstr(name,"$")!=NULL)
    {
        name +=1;
    }

    if (valName(name) == false)//Checking if the name contains illegal chars
    {
        printf ("Name of variables contains illegal characters!\n");
        addVariable("EXITCODE", "-1");
        return;
    }

    int index = searchByVarName(name); // Using the searchByVarName() function index is either >=0 (var exists) or -1 (var doesnt exist).

    if(index != -1) //Variable already exists (overwrite).
    {
       // printf("Overwriting current value of, %s\n.", name);
        VarArr->arr[index]->data = data;
        return;
    }else if (index == -1 && current != 0){//Adding a new variable to the array.
        //printf("Adding new variable, %s.\n",name);

        VarArr->arr = (Var **)realloc(VarArr->arr, sizeof(Var)*current);//NB: realloc used to resize already established blocks of memory.
    }
        VarArr->arr[current] = malloc(sizeof(Var));
        VarArr->arr[current]->name= name;
        VarArr->arr[current]->data= data;
        VarArr->varNo ++;
        return;
}

//Input: Name of a variable
//Output: Integer corresponding the index of inputted variable name (-1) if not found.
//Function: Returns to index of inputted variable names and can  be used for checking that variable names are not repeated.
int searchByVarName(char * name)
{
    for (int j=0;j<VarArr->varNo;j++)
    {
        if (strcmp(VarArr->arr[j]->name,name)==0)
        {
            return j;
        }
    }
    return -1;
}


//Input:\
//Output:\
//Function: Initialise all variables for the proper running of the eggshell.
void initVar()
{
    VarArr = malloc(sizeof(Arr));//Allocating memory for array.
    VarArr->varNo = 0;//initialises number of variables (updated by the addVariable() function).

    VarArr->arr = calloc(1, sizeof(Var));//Allocates memory for the first variable in the array.

    //Error Code
    addVariable("EXITCODE","NULL");
  
    //Environment Variables
    addVariable("PATH", getenv("PATH"));
    addVariable("USER", getenv("USER"));
    addVariable("HOME", getenv("HOME"));

    //Setting PROMPT, CWD, SHELL and TERMINAL (see respective functions)
    CWD();
    PROMPT();
    SHELL();
    addVariable("TERMINAL", ttyname(STDIN_FILENO));



}

//Input:\
//Output:\
//Function: Gets current working directory and adds it to the variables array.
void CWD()
{
    char temp[MAX_STRING] = "";//temporary buffer
    char * pt=NULL;//pointer for variable

    getcwd(temp, sizeof(temp));//gets current working directory and stores it in the temp variable
    pt = strdup(temp);//pointing 'pt' to 'temp'.

    addVariable("CWD", pt);
}

//Input:\
//Output:\
//Function: Sets the PROMPT variable
void PROMPT()
{
    char temp[MAX_STRING] = "";
    strcat(temp, "eggshell-1.0 > ");
    char * pt = strdup(temp);
    
    addVariable("PROMPT", pt);
}

//Input:\
//Output:\
//Function: Assigns path for shell (to current binary file).
void SHELL()
{
    char temp[MAX_STRING];
    char * pt = malloc(MAX_STRING * sizeof(char));

    pid_t tempPid = getpid();
    sprintf(temp, "/proc/%d/exe",tempPid);

    readlink(temp, pt, sizeof(pt));
    addVariable("SHELL",pt);
}


//Input: Variable name
//Output: Variable data
//Function: To return the data of a variable as prompted by its name
char * returnVarData(char * Vname)
{
    if(strstr(Vname,"$")!=NULL)
    {
        Vname +=1;
    }
   // printf("%s\n", Vname);
    for (int j=0;j<VarArr->varNo;j++)
    {
        if (strcmp(VarArr->arr[j]->name,Vname)==0)
        {
            return VarArr->arr[j]->data;
        }
    }

    printf("Variable not found!\n");
    addVariable("EXITCODE","-1");

    return "NULL";
}


//Input: \
//Output: \
//Function: print all variables
void printAllVar(void)
{
    for (int i=0;i<VarArr->varNo;i++)
    {
        printf("%s=%s\n",VarArr->arr[i]->name,VarArr->arr[i]->data);
    }
}

//Input: \
//Output: \
//Function: Edits or Adds a new Variable by User
void writeToVar(char * NewName, char * NewData)
{
    addVariable(NewName,NewData);
}

//Input: Variable name
//Output: \
//Function: Removes variable with given name.
void removeVarByName(char * name)
{
    if(strstr(name,"$")!=NULL)
    {
        name +=1;
    }

   int d = searchByVarName(name);//searchVarByName returns the index of the element if it exists and -1 otherwise.

   if(d == -1)//if variable doesnt exist.
   {
    printf("Error variable does not exist!\n");
    return;
   }

   else if(d >= 0 && d+1 != VarArr->varNo)//if variable exists but isnt the last element in the array (hence other elements must first be shifted).
   {
    for (int i =d+1; i<VarArr->varNo;i++)
    {
        //Shifting elements
        VarArr->arr[i-1]->name = VarArr->arr[i]->name;
        VarArr->arr[i-1]->data = VarArr->arr[i]->data;
    }
    VarArr->varNo --;//decrement varNo.
    int current =VarArr->varNo;
    VarArr->arr = (Var **)realloc(VarArr->arr, sizeof(Var)*current);//using realloc to resize the array appropriately.
   }
   else if (d+1 == VarArr->varNo)//if variable exists and is the last element in the array.
   {
    VarArr->varNo --;
    int current =VarArr->varNo;
    VarArr->arr = (Var **)realloc(VarArr->arr, sizeof(Var)*current);
   }
}

//Input:\
//Output:\
//Function: free memory allocated by initVar
void freeMem()
{
    free(VarArr);
}