
/* Functions for the smooth running of IO redirection and Piping processes*/

#include "eggShellHead.h"

//Input: pointer to array of arguments and number of arguments in said array.
//Output:\
//Function: Parses out the arguments in the args array into another array separating arguments respective to any pipes or IO redir.
void checkForPipe(char * args[MAX_ARGS], int tokenIndex)
{
    char * pipe[MAX_ARGS][MAX_ARGS]; //2D array used for separating array of args into separate commands as indicated by any pipes.
    int x=0;
    int y=0;
    int noPipes=0;

    for (int i=0;i<tokenIndex; i++) {
        if (strcmp(args[i], "|") == 0)//case when pipe is found
        {
            pipe[x][y] = NULL;//current position in row set to NULL (indicated end of command line before pipe).
            //jumping to next row.
            y = 0, x++;
            noPipes++;//variable keeping track of number of pipes incremented.

        }else{
            //push commands to current row.
            pipe[x][y]= args[i];
            y++;//increment y variable.
        }
    }
    pipe[x][y]=NULL;//once end of tokens is reached set current position to NULL indicating end of command line.
    x++;// x variable incremented.

    if(x>1) pipeline(pipe,noPipes);//If pipes exist pass to pipeline function

    else
    {
        //else pass to IO parser.
        checkIO(pipe[0]);
    }
}


void pipeline(char * arr[MAX_ARGS][MAX_ARGS], int noPipes)
{
//Variables to run piping.
int fd[2*noPipes];
int noCmds = noPipes+1;
int currPipe =0;
pid_t pid;

//printf("%d\n",noPipes);

for (int i =0; i<noPipes;i++)
{
    if(pipe(fd+i*2)<0)//Creates all pipes and caters for pipe creation errors.
    {
        fflush(stdout);
        printf("pipe() failed to create needed pipes.\n");
        writeToVar("EXITCODE","-1");
        return;
    }
}

int j;
for (j=0; arr[j][0] !=NULL; j++ )//for loop used for setting up pipeline.
{
    if((pid=fork())<0)
    {
        fflush(stdout);
        printf("Fork failed!\n");
        addVariable("EXITCODE","-1");
        return;
    }

    if(pid==0)
    {
        if(j != noCmds-1)//unless we are at the last command, we set the current command to write to the next .
        {
            if(dup2(fd[currPipe+1],STDOUT_FILENO)<0)//If the old file descriptors are not valid dup2 returns a negative int indicating failure.
            {
                fflush(stdout);
                printf("Pipe failed!");
                addVariable("EXITCODE","-1");
                exit(EXIT_FAILURE);//terminates calling process unsuccessfully
            }
        }

        if (j != 0)//unless we are at the first command, we set the current command to read from the previous.
        {
            if(dup2(fd[currPipe-2],STDIN_FILENO)<0)
            {
                fflush(stdout);
                printf("Pipe failed!");
                addVariable("EXITCODE","-1");
                exit(EXIT_FAILURE);//terminates calling process unsuccessfully
            }
        }

        for(int i=0; i<noPipes*2;i++)//closing any open pipes.
        {
            close(fd[i]);
        }

        checkIO(arr[j]);//passes to check for io redir

        exit(0);//terminates calling process successfully 
    }
    currPipe+=2;
}

for(int i =0; i<(2*noPipes);i++)//closes any open fd's after all pipes have been performed.
{
    close(fd[i]);
}

int status;
for (int i=0; i<=noPipes;i++)//wait for children processes.
{
    wait(&status);
}

}


void checkIO(char * args[MAX_ARGS])
{
    char * cmd[MAX_ARGS]={NULL};

    int redir = 0;//variable for flagging redir according to symbol
    int i;

    for(i =0; redir ==0 && args[i] != NULL;i++)//parse cmd line for redir symbols.
    {
        if (strcmp(args[i],">")==0) redir =1;

        else if (strcmp(args[i],">>")==0) redir =2;

        else if (strcmp(args[i],"<")==0) redir=3;

        //note the redir variable is set to 1, 2 or 3 depending on the redir symbol.

        else cmd[i]=args[i];
    }

    if((redir ==1 || redir ==2 || redir == 3)&& args[i+1] != NULL)//if more than one filename is given for redirection.
    {
        fflush(stdout);
        printf("Enter a single file name for redirection!\n");
        addVariable("EXITCODE", "-1");
        return;
    }

    if(redir != 0)
    {
        if(redir ==1 || redir == 2)
        {
            //redirectoutput
            redirOut(args[i],redir,cmd);
        }else if( redir == 3){
            //redir input
            redirInput(args[i],cmd);
        }
    }else{
        //pass to command parser
        parseCmd(cmd);
    }

}

void redirOut(char * name, int redir, char * cmd[MAX_ARGS])
{
    
    fflush(stdout);//flushing buffer.
    int fd1;

    
    int fd2 = dup(STDOUT_FILENO);
    if(fd2 <0)
    {
        //failed to point fd2 to standard output.
        fflush(stdout);
        printf("Failed to duplicate stdout!\n");
        addVariable("EXITCODE", "-1");
         return;
    }

    //using fd1 to create or open file to be written (redir = 1) to, or appended to (redir = 2). 

    if ( redir == 1)
    {
        fd1 = open (name, O_WRONLY | O_CREAT | O_TRUNC);//opens file for overwritting
    }else if (redir == 2)
    {
        fd1 = open(name, O_WRONLY | O_CREAT | O_APPEND);//opens file to append
    }


    if ( fd1 < 0)
    {
        //failed to open/create file
        fflush(stdout);
        printf("Failed to Open/Create File!\n");
        addVariable("EXITCODE", "-1");
         return;
    }


    if(dup2(fd1, STDOUT_FILENO)<0)
    {
        fflush(stdout);
        printf("Failed to duplicate stdout to File Descriptor!\n");
        addVariable("EXITCODE", "-1");
         return;
    }

    close(fd1);

    parseCmd(cmd);

    fflush(stdout);

    if(dup2(fd2, STDOUT_FILENO)<0)
    {
        fflush(stdout);
        printf("Failed to duplicate stdout to File Descriptor!\n");
        addVariable("EXITCODE", "-1");
         return;

    }
 
    printf("Output successfully redirected to file.\n");
     close(fd2);

    return;

}

void redirInput(char *name, char * cmd[MAX_ARGS])
{
    fflush(stdin);//flushing the standard input 
    int fd1 = open(name, O_RDONLY);//opening file for reading
    int fd2 = dup(STDIN_FILENO);//connecting fd2 to standard input.

    if(fd2 <0)
    {
        fflush(stdout);
        printf("Failure to duplicate standard input!\n");
        addVariable("EXITCODE", "-1");
        return ;
    }

    if (fd1 < 0)//unable to open file.
    {
        fflush(stdout);
        printf("File unable to open!\n");
        addVariable("EXITCODE", "-1");
        return ;
    }

    if (dup2(fd1 ,STDIN_FILENO)<0)//conncting fd1 to standard input.
    {
        fflush(stdout);
        printf("Failure to duplicate standard input!\n");
        addVariable("EXITCODE", "-1");
        return;
    }

    close(fd1);   

    parseCmd(cmd);

    fflush(stdin);//flushing standard input.

    if(dup2(fd2, STDIN_FILENO)<0)//reconnect standard input.
    {
        fflush(stdout);
        printf("Error failed to reconnect STDIN\n");
        addVariable("EXITCODE", "-1");
        return;
    } 

    close(fd2);
}